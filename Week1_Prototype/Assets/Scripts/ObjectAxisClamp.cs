﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAxisClamp : MonoBehaviour
{
    private Rigidbody _rb;

    private Vector3 _cubeVelocity;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
        _cubeVelocity = _rb.velocity;
        if (_cubeVelocity == Vector3.zero)
        {
            _rb.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
        if (_cubeVelocity.z >= .1)
        {
            _rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
        
        if (_cubeVelocity.x >= .1)
        {
            _rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }
}
